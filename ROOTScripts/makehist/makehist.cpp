#include <cstdio>
#include <cstdlib>
#include <stdio.h>
#include <ctime>

#ifndef _MSC_VER
    typedef char _TCHAR;
#else
	#if (_MSC_VER >= 1700) 
		#include <tchar.h>
	#else
        typedef char _TCHAR;
	#endif
#endif

#include <TTree.h>
#include <TFile.h>
#include <TParameter.h>
#include <TMath.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <THStack.h>

#include "../common.h"

int makehist(const int runnum = -1, int nbins = 50, const char includeChannels[] = "0123456", int take = -1, int onlyGoodEvents = 0, int minx = -1, int maxx = -1)
{
	uint16_t fixed_pedestal[8];

    if (runnum < 0) {
        printf("Makehist plots histograms for all events in run #runnum.\n\n" \
               "Syntax:\n\nmakehist(int runnum[, int nbins = 50[, str channels[, int samplesForPedestal = -1[, onlyGoodEvents = 0[, minX = -1[, maxX = -1]]]]]])\n\nTo calculate nbins or samplesForPedastal automatically, enter -1.\n\n'Channels' allows control over which " \
               "channels to display in the histogram.\n\nFor example, passing \"014\" (in quotes) displays channels 0, 1, and 4 only. The default is to display all channels.\n\n"\
               "OnlyGoodEvents takes 1 or 0 (default 0), meaning events with unclean presamples ranges, attenuated signals etc. are ignored.\n\n" \
               "For run 255 with 250 bins displaying only channels 0,1,4: makehist(255, 250, \"014\").\n\n");
        return 1;
    }
    if (nbins < 0) nbins = 50;
	if (take == -2) {
		char inputBuffer[100];
		for (int channelNumber = 0;channelNumber<7;channelNumber++) {
			printf("input fixed pedestal height for channel %d:",channelNumber);
			gets(inputBuffer);
			fixed_pedestal[channelNumber] = atoi(inputBuffer);
		}
	}

    FILE* datainput;
    char datafiledir[256];
	memset(datafiledir, 0, 256);
    const char* configfilename = "makehist.cfg";

    // Take data files directory from a local config file.
    FILE* configfile;
    configfile = fopen(configfilename,"r");
    if (configfile == NULL) {
        printf("Enter data file directory >");
        gets(datafiledir);
        configfile = fopen(configfilename, "w+");
        fwrite(datafiledir,255,1,configfile);
        fclose(configfile);
    } else {
        fread(datafiledir,255,1,configfile);
        fclose(configfile);
    }

    // Convert runnum to filename
    char filename[256];
    char ROOTFileName[256];
    char AnalysisROOTFileName[256];
    char LogFileName[256];
    char histogramROOTFileName[256];

    uint32_t timeval = time(NULL);
    sprintf(filename, "%s/psi13_%06d.bin", datafiledir, runnum);
    sprintf(ROOTFileName,"%s/psi13_%06d.root", datafiledir, runnum);
    sprintf(AnalysisROOTFileName,"%s/psi13_%06d.analysis.root", datafiledir, runnum);
    sprintf(LogFileName,"%s/psi13_%06d.log", datafiledir, runnum);
    sprintf(histogramROOTFileName,"%s/psi13_%06d.histogram.root", datafiledir, runnum, timeval);

	FILE* LogFile = fopen(LogFileName,"w");
	if (LogFile == NULL) {
		printf("Logfile not opened!!!!!\n");
		return -1;
	}

    // Check if ROOT file already exists, if it does skip the data storage. <- This should be its own programme really, any takers to make it so?
	FILE* CROOTFile = fopen(ROOTFileName,"r");
    if (CROOTFile == NULL) { // file doesn't exist
		//check if binary raw data file exists. To change the directory edit/delete the makehist.cfg
        datainput = fopen(filename,"rb");
        if (datainput==NULL) {fputs ("Couldn't open data file. Are you sure it actually exists?\n", stderr); return -8;}

		fprintf(LogFile,"Binary file: %s\n", filename);
		fprintf(LogFile,"Raw ROOT output: %s\n", ROOTFileName);
		fprintf(LogFile,"Histogram ROOT output: %s\n\n", histogramROOTFileName);

        FileHeader fh;
        fread((void*)&fh, sizeof(FileHeader), 1, datainput);
        printf("RunNumber = %i, SamplingFrequency = %i, Presamples = %i\n", fh.RunNumber , fh.SamplingFrequency, fh.Presamples);
		fprintf(LogFile, "RunNumber: %i\nSamplingFrequency: %i\nPresamples: %i\n", fh.RunNumber , fh.SamplingFrequency, fh.Presamples);
     
		EventHeader eh;
        uint16_t * eb;
        unsigned int elements;

        TFile *ROOTFile = new TFile(ROOTFileName, "RECREATE");
        TTree *T = new TTree("Tree","Run");

        TParameter<int> RunNumber("RunNumber", fh.RunNumber);
        T->GetUserInfo()->Add(&RunNumber);
        TParameter<int> Presamples("Presamples", fh.Presamples);
        T->GetUserInfo()->Add(&Presamples);
        TParameter<int> SamplingFrequency("SamplingFrequency", fh.SamplingFrequency);
        T->GetUserInfo()->Add(&SamplingFrequency);

        //Get information from first event and 
        uint32_t Samples, Channels;
		// Read in the event header
        fread((void*)&eh,sizeof(EventHeader),1,datainput);
        Samples = eh.Samples;
        Channels = eh.Channels;
        printf("\nChannels %d, Samples %d",Channels,Samples);
		fprintf(LogFile, "Channels: %i\nSamples: %i\nReading data...\n", Channels, Samples);
     
        // Reset position in file.
        fseek(datainput, sizeof(FileHeader), SEEK_SET);

		//Add ROOT branches by telling ROOT the pointers to the values to be written
        T->Branch("EventNumber",&eh.Number,"EventNumber/i");
        T->Branch("EventTime",&eh.Time,"Time/i");
        T->Branch("FPGANumber",&eh.FPGAEvent,"FPGANumber/i");
        T->Branch("FPGATime",&eh.FPGATime,"FPGATime/i");
        T->Branch("Type",&eh.Type,"Type/i");
        T->Branch("Channels",&eh.Channels,"Channels/i");
        T->Branch("Samples",&eh.Samples,"Samples/i");

		//Add branches for the up to 7 channels
        char channelname[256],channelname_t[256];
        eb = new uint16_t[Samples * Channels];
        for (unsigned int i=0;i<Channels;i++) {
            sprintf(channelname, "Channel%d",i); //name
            sprintf(channelname_t, "Channel%d[Samples]/s",i); //type
            T->Branch(channelname, eb + i * Samples, channelname_t);
        }

		uint32_t EventsRead = 0;

        while(!feof(datainput)) {
            elements = fread((void*)&eh, sizeof(EventHeader), 1, datainput);
            if (elements > 0) {
				// Read the binary data directly into memory, structured with the event structure
                fread((void*)eb, sizeof(uint16_t), Samples * Channels, datainput);
				// Populate the tree.
                T->Fill(); // Segmentation violation here!
				EventsRead++;
            }
        }
		fprintf(LogFile, "Events read: %d\n", EventsRead);
		fflush(LogFile);

        fclose(datainput);
        ROOTFile->Write();

        ROOTFile->Close();

		fprintf(LogFile, "ROOT file written.\n");
     
        printf("\n\nRaw data stored in ROOT file '%s'\n", ROOTFileName);
    } else {
        printf("\nROOT file already exists...Skipping.\n\n");
    }
	if (CROOTFile != NULL) fclose(CROOTFile);
	
    TFile *AnalysisROOTFile = new TFile(AnalysisROOTFileName, "RECREATE");
	TTree *AnalysisTree;
	eventAnalysis ea[8];
	
	AnalysisTree = new TTree("AnalysisTree","AnalysisTree");
	for (int channelNumber=0;channelNumber<7;channelNumber++) {
		char branchName[256],branchType[256];
		sprintf(branchName,"Ch%d_Integral",channelNumber);
		sprintf(branchType,"Ch%d_Integral/I",channelNumber);
		AnalysisTree->Branch(branchName,&ea[channelNumber].integral,branchType);
		sprintf(branchName,"Ch%d_Maximum",channelNumber);
		sprintf(branchType,"Ch%d_Maximum/I",channelNumber);
		AnalysisTree->Branch(branchName,&ea[channelNumber].maximum,branchType);
		sprintf(branchName,"Ch%d_Pedestal",channelNumber);
		sprintf(branchType,"Ch%d_Pedestal/I",channelNumber);
		AnalysisTree->Branch(branchName,&ea[channelNumber].pedestal,branchType);
		sprintf(branchName,"Ch%d_SignalStart",channelNumber);
		sprintf(branchType,"Ch%d_SignalStart/I",channelNumber);
		AnalysisTree->Branch(branchName,&ea[channelNumber].signal_start,branchType);
		sprintf(branchName,"Ch%d_Flag",channelNumber);
		sprintf(branchType,"Ch%d_Flag/I",channelNumber);
		AnalysisTree->Branch(branchName,&ea[channelNumber].Flag,branchType);
		sprintf(branchName,"Ch%d_PedestalSTDDEV",channelNumber);
		sprintf(branchType,"Ch%d_PedestalSTDDEV/I",channelNumber);
		AnalysisTree->Branch(branchName,&ea[channelNumber].PedestalSTDDEV,branchType);
		sprintf(branchName,"Ch%d_PedestalDiff",channelNumber);
		sprintf(branchType,"Ch%d_PedestalDiff/I",channelNumber);
		AnalysisTree->Branch(branchName,&ea[channelNumber].PedestalDiff,branchType);
		sprintf(branchName,"Ch%d_PedestalMaxVariation",channelNumber);
		sprintf(branchType,"Ch%d_PedestalMaxVariation/I",channelNumber);
		AnalysisTree->Branch(branchName,&ea[channelNumber].PedestalMaxVariation,branchType);
	}
    

	printf("Now make histograms\n\n");
    // Now make histograms.
    // This is for choosing the channels to display.
    int l = 0;
    int displayChannelsArr[7];
	displayChannelsArr[0] = -1;
    for (int k=0;k<7;k++) {
        for (int m=0; m<strlen(includeChannels); m++) {
            if(includeChannels[m] - '0' == k) {
                displayChannelsArr[l++] = k;
            }
        }
    }
	fprintf(LogFile, "Channels to include in histogram: %s\n", includeChannels);

	printf("Load ROOT Tree\n");
	// Load the tree of raw data we just created and saved.
    TFile* input;
    input = new TFile(ROOTFileName, "READ");
    TTree* t = (TTree*)input->Get("Tree");
    if (t == 0) return -6;
	fprintf(LogFile, "ROOT file openened\n");
	printf("ROOT file openened\n");

    printf("%i entries\n", t->GetEntries());
	fprintf(LogFile, "Entries in tree: %i\n", t->GetEntries());

	
	printf("Getting branches...");
    EventHeader eh;
    t->SetBranchAddress("EventNumber", &eh.Number);
    t->SetBranchAddress("Type", &eh.Type);
    t->SetBranchAddress("EventTime", &eh.Time);
    t->SetBranchAddress("FPGANumber", &eh.FPGAEvent);
    t->SetBranchAddress("FPGATime", &eh.FPGATime);
    t->SetBranchAddress("Channels", &eh.Channels);
    t->SetBranchAddress("Samples", &eh.Samples);

    uint16_t channel[7][2048];
    t->SetBranchAddress("Channel0", channel[0]);
    t->SetBranchAddress("Channel1", channel[1]);
    t->SetBranchAddress("Channel2", channel[2]);
    t->SetBranchAddress("Channel3", channel[3]);
    t->SetBranchAddress("Channel4", channel[4]);
    t->SetBranchAddress("Channel5", channel[5]);
    t->SetBranchAddress("Channel6", channel[6]);
	
	printf("done\n");

    TParameter<int> *rn = (TParameter<int>*) t->GetUserInfo()->FindObject("RunNumber");
    printf("Run number: %d\n", rn->GetVal());

    TParameter<int> *presamples = (TParameter<int>*) t->GetUserInfo()->FindObject("Presamples");
    printf("Presamples: %d\n", presamples->GetVal());

    // Take is the number of samples to take for pedestal.
    // Calculate it
    if (take == -1) { // < -2 means used fixed pedestal value!!!
        take = 24;
    }

	if (take >= presamples->GetVal()) {
		 printf("\nWARNING: Number of samples for pedestal subtraction  %d >= %d number of presamples!!!!\n\n", take, presamples->GetVal());
		 fprintf(LogFile, "\nWARNING: Number of samples for pedestal subtraction  %d >= %d number of presamples!!!!\n\n", take, presamples->GetVal());
	}


    t->GetEntry(0);
    signed int integrated_val; // Cannot be of type other than signed int.
    float pedestal = 0,end_pedestal = 0;

    // Calculate min and max x values automatically (this is surely optimizable).
    if (minx == -1 || maxx == -1) {
		minx=-50000;
		maxx=450000;
    }

    TCanvas *c1 = new TCanvas("c1","Energy Histogram", eh.Samples, 10, 900, 700);
    TH1F *th[7];

    for (int k=0;k<7;k++) {
        char ChannelName[16];
        char HistogramName[16];
        sprintf(ChannelName, "Channel %i", k);
        sprintf(HistogramName, "th[%i]", k);
        th[k] = new TH1F(HistogramName, ChannelName, nbins, minx, maxx);
        th[k]->SetLineColor(k+1);
        th[k]->SetLineWidth(2);
    }
	fprintf(LogFile, "EVENT\tCH\tINT\tMAX\tPED\tENDPED\tCHNG\tSTD\tCLAMPED\tNEGATIVE\n");

	bool isBadEvent = false, isUglyEvent = false;
	uint32_t goodEventsCount = 0;
	uint32_t badEventsCount = 0;
	uint32_t uglyEventsCount = 0;
	uint32_t totalEventsCount = 0;

    const int NOISE_TOLERANCE = 10;
	
	int signalStart = 0;
    uint32_t startPedestal = 0, endPedestal = 0;
	uint32_t pedestalVarianceSum = 0;
	uint32_t pedestalVariation = 0;
	uint32_t pedestalMaxVariation = 0;
	float pedestalSTD = 0, pedestalChange = 0;

    for (int entryNumber = 0; entryNumber < t->GetEntries(); entryNumber++) {
        for (int k=0;k<7;k++) {
            t->GetEntry(entryNumber);

            integrated_val = 0;
			int negativeCount = 0;

            // Check CLAMPING
			if (take == -2) { //fixed
				startPedestal = fixed_pedestal[k];
				endPedestal = fixed_pedestal[k];
				pedestalSTD = 0;
				pedestalChange = 0;
			} else { //take first n samples
				pedestalSTD = 0;
				pedestalChange = 0;
				startPedestal = 0;
				endPedestal = 0;
				pedestalMaxVariation = 0;
				pedestalVarianceSum = 0;
				for (int j=0;j<take;j++) {
					startPedestal += channel[k][j];
					endPedestal += channel[k][eh.Samples - j - 1];
				}
				startPedestal /= take; 
				endPedestal /= take; 

				//calculate standard deviation
				for (int j=0;j<take;j++) {
					pedestalVariation = (channel[k][j] - startPedestal);
					if (abs((int)pedestalVariation) > pedestalMaxVariation) {
						pedestalMaxVariation = abs((int)pedestalVariation);
					}
					pedestalVarianceSum += pedestalVariation * pedestalVariation;
				}
				pedestalSTD = sqrt((float)pedestalVarianceSum / (take + 1));
				pedestalChange = ((float)endPedestal-(float)startPedestal)/startPedestal;
			}

			//check CLAMPING and SIGNAL START (signal > 110% of pedestal)
			uint32_t ClampedValues = 0;
			uint32_t maximum = 0;
			for (int j=0;j<eh.Samples;j++) {
				if (channel[k][j] > 16350) ClampedValues++;
				if (channel[k][j] < pedestal - NOISE_TOLERANCE) negativeCount++;
				if (channel[k][j] > maximum) maximum = channel[k][j];
				if (signalStart == 0 && channel[k][j] > pedestal * 1.1f) signalStart = j;
			}
			if (ClampedValues > 10 || abs(pedestalChange) > 0.001f || pedestalSTD > (float)NOISE_TOLERANCE) {
				isBadEvent = true;
				badEventsCount++;
			} else {
				isBadEvent = false;
				goodEventsCount++;
			}

            // Integrate
            for (int sampleNumber = presamples->GetVal(); sampleNumber < eh.Samples; sampleNumber++) {
                integrated_val += channel[k][sampleNumber] - startPedestal;
            }
			
			fprintf(LogFile, "%i\t%i\t%i\t%i\t%i\t%i\t%.4f\t%.2f\t%i\t%i", entryNumber, k, integrated_val, maximum, pedestal, end_pedestal, pedestalChange, pedestalSTD, ClampedValues, negativeCount);
			
			//Event sanity check
			if (isBadEvent == false && integrated_val < 0) {
				uglyEventsCount++;
				fprintf(LogFile, "\t UGLY\n");
				isUglyEvent = true;
			} else if (isBadEvent) {
				fprintf(LogFile, "\t BAD\n");
				isUglyEvent = false;
			} else {
				fprintf(LogFile, "\t GOOD\n");
				isUglyEvent = false;
			}

            if (onlyGoodEvents == 0 || (isBadEvent == false && isUglyEvent == false))  {
                th[k]->Fill(integrated_val); // Add to histogram.
			}

			ea[k].integral = integrated_val;
			ea[k].maximum = maximum;
			ea[k].pedestal = startPedestal;
			ea[k].signal_start  = signalStart;
			if (isBadEvent) {
				ea[k].Flag = -2;
			} else if (isUglyEvent) {
				ea[k].Flag = -1;
			} else {
				ea[k].Flag = 0;
			}
			ea[k].PedestalDiff = ((float)endPedestal-(float)startPedestal);
			ea[k].PedestalSTDDEV = pedestalSTD;
			ea[k].PedestalMaxVariation = pedestalMaxVariation;
			totalEventsCount++;
        }
		AnalysisTree->Fill();
    }

	fprintf(LogFile, "\n\n\nGOOD = all checks fulfilled\nBAD = at least one not fulfilled\nUGLY = all checks fulfilled, but still eg. negative energy\n\n");
	fprintf(LogFile, "TOTAL:%d\nGOOD:%d %f %%\nBAD:%d %f %%\nUGLY:%d %f %%\n", totalEventsCount, goodEventsCount, (float)goodEventsCount/totalEventsCount*100, badEventsCount, (float)badEventsCount/totalEventsCount*100, uglyEventsCount, (float)uglyEventsCount/totalEventsCount*100);

	printf("\n\n\nGOOD = all checks fulfilled\nBAD = at least one not fulfilled\nUGLY = all checks fulfilled, but still eg. negative energy\n\n");
	printf("TOTAL:%d\nGOOD:%d %f %%\nBAD:%d %f %%\nUGLY:%d %f %%\n", totalEventsCount, goodEventsCount, (float)goodEventsCount/totalEventsCount*100, badEventsCount, (float)badEventsCount/totalEventsCount*100, uglyEventsCount, (float)uglyEventsCount/totalEventsCount*100);

    // Histogram title
    char histogramTitle[128];
    sprintf(histogramTitle, "Energy Histograms for Run %i", rn->GetVal());

	// This allows us to display multiple histograms on the same chart.
    THStack *s = new THStack("histstack", histogramTitle);

    // Only plot those passed to the function in the variable displayChannels.
    for (int k=0;k<l;k++) {
        s->Add(th[displayChannelsArr[k]]); // Add each histogram to the histogram stack
    }

    // Draw the stack, but superimpose them: don't add them together.
    s->Draw("nostack");

    // Create and add axes labels.
    char nt[256];
    sprintf(nt, "Counts / (%d area unit)", (maxx-minx)/nbins);
    s->GetYaxis()->SetTitle(nt);
    s->GetXaxis()->SetTitle("Area"); // Is it really nVs?? no.
    c1->BuildLegend(0.85,0.85,1,1);
    c1->Update();

	// Save the histogram in a ROOT file. Also, optionally as an image, eg png, just uncomment the relevant stuff.
    //c1->SaveAs(histogramFileName);
    c1->SaveAs(histogramROOTFileName);
    printf("\n\nHistogram stored in %s.\n", histogramROOTFileName);

	// Suppress ROOT's otherwise-incessant whining.
    input->Close();
	fclose(LogFile);
	AnalysisROOTFile->Write();
	AnalysisROOTFile->Close();

    return 0;
}


int _tmain(int argc, _TCHAR* argv[])
{
	if (argc == 1) {
		int RunNumber,Bins=500,Take=24,Minx=-1,Maxx=-1,OnlyGoodEvents=0;
		char input[100];
		char Channels[100];

		printf("Run number>");
		memset(input, 0, 100);
		fgets(input, 100, stdin);
		if (strlen(input) < 2) {
			return -1;
		}
		RunNumber = atoi(input);

		printf("Bins (default = 2000)>");
		memset(input, 0, 100);
		fgets(input, 100, stdin);
		Bins = atoi(input);
		if (strlen(input) < 2) Bins = 2000; //there is always at least the \n character in the input

		printf("Channel>");
		memset(input, 0, 100);
		fgets(input, 100, stdin);
		if (strlen(input) < 2) {
			strcpy(Channels, "0123456\x00");
		} else {
			strcpy(Channels, input);
		}

		printf("Samples for pedestal (-2 = fixed)>");
		memset(input, 0, 100);
		fgets(input, 100, stdin);
		Take = atoi(input);
		if (strlen(input) < 2) Take = 24;

		printf("Min (default = -50000)>");
		memset(input, 0, 100);
		fgets(input, 100, stdin);
		Minx = atoi(input);
		if (strlen(input) < 2) Minx = -1;

		printf("Max (default = 450000)>");
		memset(input, 0, 100);
		fgets(input, 100, stdin);
		Maxx = atoi(input);
		if (strlen(input) < 2) Maxx = -1;

		printf("Only good events (1=true, 0=false, default 0)>");
		memset(input, 0, 100);
		fgets(input, 100, stdin);
		OnlyGoodEvents = atoi(input);
		if (strlen(input) < 2) OnlyGoodEvents = 0;

		fflush(stdin);
		makehist(RunNumber,Bins,Channels,Take,Minx,Maxx,OnlyGoodEvents);
		printf("[Press any key to continue]\n");
		getchar();
	}
	return 0;
}
