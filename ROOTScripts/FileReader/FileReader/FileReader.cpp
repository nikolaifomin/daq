// FileReader.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//


#include <cstdio>
#include <cstdlib>
#include <math.h>

#ifndef _MSC_VER
    typedef char _TCHAR;
#else
    #if (_MSC_VER >= 1700)
        #include <tchar.h>
    #else
        typedef char _TCHAR;
    #endif
#endif

#define _CRT_SECURE_NO_WARNINGS

#include <TTree.h>
#include <TFile.h>
#include <TParameter.h>
#include <TMath.h>
#include <TObject.h>
#include <TStorage.h>

//using namespace TMath;

//#include <cstdint>

typedef signed char        int8_t;
typedef short              int16_t;
typedef int                int32_t;
typedef long long          int64_t;
typedef unsigned char      uint8_t;
typedef unsigned short     uint16_t;
typedef unsigned int       uint32_t;
typedef unsigned long long uint64_t;


typedef struct fileHeaderStruct {
    uint32_t RunNumber;
    uint32_t SamplingFrequency;
    uint32_t Presamples;
    uint32_t reserved[61];
} FileHeader;

typedef struct eventHeaderStruct {
    uint32_t Number;
    uint32_t Type;
    uint32_t Time;
    uint32_t reserved;
    uint32_t FPGAEvent;
    uint32_t FPGATime;
    uint32_t Channels;
    uint32_t Samples;
} EventHeader;

int FileReader(const char* filename)
{
    FILE* input;
    uint32_t LastEventNumber = 0;
    bool HasWarnings = false;

    input = fopen(filename,"rb");
    if (input==NULL) {
        printf("file:%s\n",filename);
        fputs ("Couldn't open data file.",stderr); exit (1);
    }
    FileHeader fh;
    fread((void*)&fh,sizeof(FileHeader),1,input);
    printf("size = %d, RunNumber = %i, SamplingFrequency = %i, Presamples = %i", sizeof(FileHeader), fh.RunNumber , fh.SamplingFrequency, fh.Presamples);
    //getchar();

    EventHeader eh;
    uint16_t * eb;
    unsigned int elements;

    //Create ROOT file
    char ROOTFileName[256];
    sprintf(ROOTFileName,"%s.root", filename);
    TFile *ROOTFile = new TFile(ROOTFileName, "RECREATE");

    //Create TTree
    TTree *T = new TTree("Tree","Run");
    TParameter<int> RunNumber("RunNumber", fh.RunNumber);
    T->GetUserInfo()->Add(&RunNumber);
    TParameter<int> Presamples("Presamples", fh.Presamples);
    T->GetUserInfo()->Add(&Presamples);
    TParameter<int> SamplingFrequency("SamplingFrequency", fh.SamplingFrequency);
    T->GetUserInfo()->Add(&SamplingFrequency);

    //Get information from first event and reset position in file
    uint32_t Samples, Channels;
    fread((void*)&eh,sizeof(EventHeader),1,input);
    Samples = eh.Samples;
    Channels = eh.Channels;
    printf("\nChannels = %d, Samples =%d\nreading file content...\n",Channels,Samples);
    rewind(input);
    fread((void*)&fh,sizeof(FileHeader),1,input);

    T->Branch("EventNumber",&eh.Number,"EventNumber/i");
    T->Branch("EventTime",&eh.Time,"Time/i");
    T->Branch("FPGANumber",&eh.FPGAEvent,"FPGANumber/i");
    T->Branch("FPGATime",&eh.FPGATime,"FPGATime/i");
    T->Branch("Type",&eh.Type,"Type/i");
    T->Branch("Channels",&eh.Channels,"Channels/i");
    T->Branch("Samples",&eh.Samples,"Samples/i");

    char channelname[256],channelname_t[256];
    eb = new uint16_t[Samples * Channels];
    for (unsigned int i=0;i<Channels;i++) {
        sprintf(channelname, "Channel%d",i);
        sprintf(channelname_t, "Channel%d[Samples]/s",i);
        T->Branch(channelname, eb + i * Samples, channelname_t);
    }

    while(!feof(input)) {
        elements = fread((void*)&eh,sizeof(EventHeader),1,input);
        if (elements > 0 && (eh.Number > LastEventNumber || LastEventNumber == 0)) {
            fread((void*)eb, eh.Samples * eh.Channels * sizeof(uint16_t), 1, input);
            T->Fill();
            LastEventNumber = eh.Number;
        } else if (elements > 0 && eh.Number <= LastEventNumber) {
            printf("Event Number is %i, but should be > %i\n", eh.Number, LastEventNumber);
            HasWarnings = true;
        }
    }

    delete[] eb;

    //getchar();
    fclose(input);

    //printf("writing file...");
    //T->Write();
    ROOTFile->Write();
    //printf("closing file...");
    ROOTFile->Close();
    //TH1F* h = new TH1F();

    //printf("END");

    printf("\n\nROOT file successfully created!\n");
    if (HasWarnings) getchar();
    return 0;
}


int _tmain(int argc, _TCHAR* argv[])
{
    if (argc == 1) {
        printf("FileReader\n\nreads in a binary file created by LabView DAQ and exports it as .root file.\n\nEnter filename>");
        char FileName[1024];
        gets(FileName);
        if (strlen(FileName) > 0) {
            FileReader(FileName);
        }
    } else if (argc == 2) {
        char* FileName = new char[wcslen(argv[1]) + 1];
        wcstombs(FileName, argv[1], wcslen(argv[1]) );
        printf("FileReader\n\nreading %s", FileName);
        FileReader(FileName);
    }
    return 0;
}
